

var posicion = 1; // -- Variable para recorrer la posición actual
var posiciones = [1,2,3,4,5,6]; // -- arreglo de números para tablero
var posicionesOcupadas = [];    // -- Arreglo para guardar las posicones aleatorias
var puntaje = 0;    // -- Variable para obtener puntaje total

cargarValores(); // -- Función para generar tablero aleatorio

setTimeout('ocultarValores()', 3000);   // Función para ocultar número de tablero

// -- Verifica el cuadro seleccionado
function verificacion (e){
    // -- Se obtiene el valor dle input seleccionado
    var valor = e[0].getAttribute('valor');
    if (valor == posicion){
        posicion ++;
        e[0].style.background="lime";
        puntaje++;
    }
    else {
        alert("Fallaste")
        e[0].style.background="red";
        puntaje--;
    }
    if((posicion -1) == posiciones.length){
        alert("Ganaste");
        puntaje = (puntaje * 10) / 6;
        $("#puntaje").val(puntaje);
    }
}

// -- Carga valores aleatorios a todos los elementos que contengan la clase 'casilla'
function cargarValores (){
    const elemtos = document.querySelectorAll('.casilla');
    elemtos.forEach(elemento => {
        var numeroAleatorio;
        while ((numeroAleatorio = numAlea (1,6)) && posicionesOcupadas.includes(numeroAleatorio));
        posicionesOcupadas.push(numeroAleatorio);
       elemento.setAttribute('valor', numeroAleatorio);
       var  decendiente = elemento.getElementsByTagName("h3");
       decendiente[0].innerHTML = numeroAleatorio;
    });
}

// -- Genera número aleatorio
function numAlea (min, max){
    return Math.round(Math.random()* (min-max) + parseInt(max));
}

// -- Oculta valores de divs
function ocultarValores(){
    const elemts = document.querySelectorAll('.casilla');
    elemts.forEach(elemento => {
        var decendiente = elemento.getElementsByTagName('h3');
        decendiente[0].innerHTML="";
    });
}

